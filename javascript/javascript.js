function setting() {
 document.getElementById("mySidenav").style.width = "100%";
 document.getElementById("theme").setAttribute('content','#141411')
 document.getElementById("mobile_menu").setAttribute('onclick','closeNav()');
 document.getElementById("navimg").setAttribute('src','icon/logo-white.png');
 $('.navbar').addClass('top');
 $('.nav_ul').addClass('top');}
function closeNav() {
 document.getElementById("mySidenav").style.width = "0px";
 document.getElementById("theme").setAttribute('content','#fff')
 document.getElementById("mobile_menu").setAttribute('onclick','setting()')
 document.getElementById("navimg").setAttribute('src','icon/logo-black.png');
 $('.navbar').removeClass('top');
 $('.nav_ul').removeClass('top');}
function refresh() {
 self['location']['reload']()}

// NAVBAR
$(window).scroll(function (event) {
var scroll = $(window).scrollTop();
 if (scroll >= 5) {
    	$('.navbar').removeClass('top');
      $('.nav_ul').removeClass('top');
      document.getElementById("navimg").setAttribute('src','icon/logo-black.png');
    } else {
    	$('.navbar').addClass('top');
      $('.nav_ul').addClass('top');
      document.getElementById("navimg").setAttribute('src','icon/logo-white.png');
    }
});

// FORMTAB
function openform(evt, formName) {
   var i, tabcontent, tablinks;
   tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(formName).style.display = "block";
    evt.currentTarget.className += " active";
}

function emptystate(){
  document.getElementById("state").style.display ="none";
}

/*Accordian controls*/
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  }
}